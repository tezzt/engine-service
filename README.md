# Build&Run C/C++

This tools is an API that build and run an exercise with a solution (within a time limit) and return its output and an error code. On success, it returns the percentage of complession of tests.

## The API

## The script that build & run

The script is `judge` and it builds using a makefile. It can be seen [in C](sandbox_os/c_compil/Makefile) or [in C++](sandbox_os/cpp_compil/Makefile).

For now, only [LAssert](https://github.com/Klevh/LAssert) is supported. If you are a maintainer, it is easy to add a supported test tool by following those steps:
* creating a script (ex: [lassert_analyzer.sh](sandbox_os/parsers/lassert_analyzer.sh)) that return 0 on success and print the percentage of success, a new line and the console output and return 1 on execution failure and print the output.
* creating a script (ex: [lassert_condition.sh](sandbox_os/parsers/lassert_condition.sh)) that return 1 if your tool is being used or 0 if not
* add both those scripts in the [parsers](sandbox_os/parsers) folder