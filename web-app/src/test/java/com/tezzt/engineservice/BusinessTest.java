package com.tezzt.engineservice;

import com.tezzt.engineservice.models.Resultat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration
public class BusinessTest {

    @Autowired
    private Business businessLayer;

    @Test
    public void testWorkingScore() {
        Resultat res = businessLayer.judge(0);

        // Assert
        assertEquals(41.66, res.getScore(), 0.01);
    }

    @Test
    public void testBuildingErrorScore() {
        Resultat res = businessLayer.judge(1);

        // Assert
        assertEquals(0.0, res.getScore(), 0.01);
    }
}
