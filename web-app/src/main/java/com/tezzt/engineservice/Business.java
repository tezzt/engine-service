package com.tezzt.engineservice;

import com.tezzt.engineservice.models.Exercice;
import com.tezzt.engineservice.models.Resultat;
import com.tezzt.engineservice.models.Solution;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.PumpStreamHandler;
import org.omg.PortableInterceptor.SUCCESSFUL;
import org.springframework.stereotype.Service;
import org.apache.commons.exec.DefaultExecutor;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.File;
import java.util.HashMap;

@Service
public class Business {

    public Resultat judge(Integer idSolution) {

        // l'output stream permet de récuperer le résultat du programme (sortie standard)
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);

        //Variable contenant le résultat à retourner
        Resultat res = null;

        //Rcupération du chemin des fichiers exercice et des fichiers de la solution
        Solution solution = getSolution(idSolution);
        Exercice exercice = solution.getExercice();

        // Le script bash utilisé utilise la syntaxe suivante : ./judge <chemin de l exo> <chemin de la solution> <id de la solution>
        String line = "./judge " + exercice.getPath() + " " + solution.getPath() + " " + solution.getId();

        //Pour plus d'infos sur l'execution de la commande cf Apache Commons Exec
        CommandLine cmdLine = CommandLine.parse(line);
        DefaultExecutor executor = new DefaultExecutor();

        //Il est nécéssaire de définir son répertoire courant afin de pouvoir executer correctement le script
	    executor.setWorkingDirectory(new File("../sandbox_os/"));
        executor.setStreamHandler(streamHandler);

        System.out.println("line : " + line);
        /*
            Deux cas de figures :

            1) Script exécuté avec succés, donc pas d'exception lévé on récupère directement le contenu de la console
            2) Erreur (compilation, execution ou timeout) dans ce cas excetion levé par ACE.
         */
        try {
            executor.execute(cmdLine);

            String msg = outputStream.toString();
            System.out.println("msg : " + msg);

            int id = msg.indexOf('\n');
            if(id != -1)
                res = new Resultat(Double.parseDouble(msg.substring(0, id)), Resultat.Statut.SUCCESS, msg.substring(id));
            else
                res = new Resultat(Double.parseDouble(msg), Resultat.Statut.SUCCESS, msg);

        } catch (ExecuteException e) {
            String msg = outputStream.toString();
            System.out.println("msg : " + msg);

            res = new Resultat(0.0, Resultat.Statut.ERROR, msg);
        } catch (IOException e) {
            res = null;
        }

        return res;
    }

    public Solution getSolution(Integer idSolution) {
        HashMap<Integer, Solution> mockSolutions = new HashMap<Integer, Solution>();

        String[] files = {"working", "buildFailure", "cppWorking", "mainoverride", "rmall", "timeout"};

        for(int i = 0; i < files.length; i++)
            mockSolutions.put(i, new Solution(i, "dataTest/" + files[i] + "/sol.zip", new Exercice("dataTest/" + files[i] + "/exo.zip")));

        return mockSolutions.get(idSolution);
    }
}
