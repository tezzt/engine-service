package com.tezzt.engineservice.models;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Resultat {
    public enum Statut {
        SUCCESS,
        ERROR
    }

    private Double score;
    private Statut statut;
    private String msg;

}
