package com.tezzt.engineservice.models;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Exercice {
    String path;
}
