package com.tezzt.engineservice.models;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Solution {
    Integer id;
    String path;
    Exercice exercice;
}
