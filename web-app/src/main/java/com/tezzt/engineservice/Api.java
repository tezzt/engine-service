package com.tezzt.engineservice;

import com.tezzt.engineservice.models.Resultat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Api {

    private Business businessLayer;

    public Api(Business businessLayer) {
        this.businessLayer = businessLayer;
    }

    @GetMapping("/judge/{idSolution}")
    public Resultat judge(@PathVariable("idSolution") Integer idSolution) {
        return businessLayer.judge(idSolution);
    }
}
