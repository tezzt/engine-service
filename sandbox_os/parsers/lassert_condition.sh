#!/bin/bash

while read -r s; do
    s="${s##*/}"
    if [ "${s}" == "LAssert.h" ]; then
        exit 1
    fi
done <<< "$*"
