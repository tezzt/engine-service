#!/bin/bash

inTest=0
totalSections=0
sumPercent=0
totalCases=0
failedCases=0

# processing output
output=
while read -r s; do
    output="${output}\n${s}"
    if [[ "${s}" =~ "------------------------------------------------------------" ]]; then
        if [ "${inTest}" == "1" ]; then
            inTest=0
            if [ "${totalCases}" != "0" ]; then
                percent=`bc -l <<< ${totalCases}-${failedCases}`
                percent=`bc -l <<< ${percent}/${totalCases}`
                percent=`bc -l <<< ${percent}*100`
                sumPercent=`bc -l <<< ${percent}+${sumPercent}`
            fi
            totalCases=0
            failedCases=0
        else
            inTest=1
            totalSections=`expr ${totalSections} + 1`
        fi
    elif [[ "${inTest}" == "1" ]] && [[ "${s}" =~ ([A-Za-z]+)" : "([1-9]+)" test_case(s)" ]]; then
        nb="${BASH_REMATCH[2]}"
        totalCases=`expr ${totalCases} + ${nb}`
        if [[ "${BASH_REMATCH[1]}" != "Succeeded" ]]; then
            failedCases=`expr ${failedCases} + ${nb}`
        fi
    fi
done

if [ "${inTest}" == 1 ]; then
    echo -e "${output}"
    exit 1
fi

# calculating result as %
if [ "${totalSections}" != 0 ]; then
    result=`bc -l <<< ${sumPercent}/${totalSections}`
else
    result="0"
fi
echo -e "${result}\n${output}"
